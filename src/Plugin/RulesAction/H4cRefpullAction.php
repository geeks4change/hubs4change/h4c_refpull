<?php

namespace Drupal\h4c_refpull\Plugin\RulesAction;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\rules\Core\RulesActionBase;

/**
 * Provides an 'H4C Reference Pull' action.
 *
 * @RulesAction(
 *   id = "h4c_refpull_action",
 *   label = @Translation("H4C Reference Pull"),
 *   category = @Translation("H4C custom"),
 *   context = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       description = @Translation("The entity in presave.")
 *     ),
 *     "target_field_name" = @ContextDefinition("string",
 *       label = @Translation("Target field name"),
 *       description = @Translation("E.g. field_domain_access.")
 *     ),
 *     "reference_field_name" = @ContextDefinition("string",
 *       label = @Translation("Reference field name"),
 *       description = @Translation("E.g. field_event_type.")
 *     ),
 *     "source_field_name" = @ContextDefinition("string",
 *       label = @Translation("Source field name"),
 *       description = @Translation("E.g. field_domains_to_add.")
 *     ),
 *   }
 * )
 */
class H4cRefpullAction extends RulesActionBase {

  /**
   * H4C Reference Pull.
   */
  protected function doExecute(EntityInterface $entity, $target_field_name, $reference_field_name, $source_field_name) {
    if (!$entity instanceof FieldableEntityInterface) {
      throw new \UnexpectedValueException('H4C Refpull expected fieldable entity.');
    }
    if ($entity->hasField($target_field_name) && $entity->hasField($reference_field_name)) {
      $currentReferenceField = $entity->get($reference_field_name);
      if (!$currentReferenceField instanceof EntityReferenceFieldItemListInterface) {
        throw new \UnexpectedValueException("H4C Refpull expected $reference_field_name to be a reference.");
      }
      $currentReferencedEntities = $currentReferenceField->referencedEntities();
      if (isset($entity->original)) {
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $originalReferenceField */
        $originalReferenceField = $entity->original->get($reference_field_name);
        $originalReferencedEntities = $originalReferenceField->referencedEntities();
        $newReferencedEntities = array_udiff($currentReferencedEntities, $originalReferencedEntities, function(EntityInterface $a, EntityInterface $b) {
          return ($a->getEntityTypeId() === $b->getEntityTypeId() && $a->id() === $b->id()) ? 0 : -1;
        });
      }
      else {
        $newReferencedEntities = $currentReferencedEntities;
      }

      // We do not uniqueify the entities-to-add, but this will be taken care of
      // when actually adding.
      $entitiesToAdd = [];
      foreach ($newReferencedEntities as $newReferencedEntity) {
        if ($newReferencedEntity instanceof FieldableEntityInterface && $newReferencedEntity->hasField($source_field_name)) {
          $sourceField = $newReferencedEntity->get($source_field_name);
          if (!$sourceField instanceof EntityReferenceFieldItemListInterface) {
            throw new \UnexpectedValueException("H4C Refpull expected $source_field_name to be a reference.");
          }
          $sourceEntities = $sourceField->referencedEntities();
          $entitiesToAdd = array_merge($entitiesToAdd, $sourceEntities);
        }
      }

      $targetField = $entity->get($target_field_name);
      if (!$targetField instanceof EntityReferenceFieldItemListInterface) {
        throw new \UnexpectedValueException("H4C Refpull expected $target_field_name to be a reference.");
      }
      $targetEntities = $targetField->referencedEntities();
      foreach ($entitiesToAdd as $entityToAdd) {
        // Use strict comparison for objects.
        if (!in_array($entityToAdd, $targetEntities, TRUE)) {
          $targetField->appendItem($entityToAdd);
        }
      }
    }
  }

}
