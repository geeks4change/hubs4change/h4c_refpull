# About h4c_refpull

- IMPORTANT: Due to dependency errors (#1), dependencies and patch have been removed. Add those to your project:

```
    "extra": {
        "patches": {
            "drupal/rules": {
              "h4c_domain / 647 / 2987505 / rules UI": "https://www.drupal.org/files/issues/2019-04-12/rules-2987505-19-Cannot-set-list-to-nonarray.patch"
            }
        }
    },
    "require": {
        "drupal/rules": "3.x-dev#78c9c13"
    }
```
